#include <iostream>

#include "queue_spsc.h"
#include <thread>
#include <future>
#include <cstdio>
#include <chrono>
#include <iostream>
#include<unistd.h>
constexpr std::size_t NumItems = 100'000'000;
using QueueType = queue_spsc<uint64_t, 512, 4096, policy::atomic>;
using QueuePoolType = queue_pool<QueueType, 8>;

void pool_producer(QueuePoolType *p) {
    static size_t busy = 0;
    for (int i = 0; i < NumItems;) {
        std::size_t next_w;
        QueueType *queue;
        auto res = p->getNext(queue);
        //queue = p->pool(0);
        typename QueueType::DataBucketPtr data_ptr;
        auto stat = queue->acquire_for_write(data_ptr, next_w);
        if (stat) {
            data_ptr->seq = i;
            data_ptr->state = QueueType::DataBucket::PROCESS;
            queue->finish_write(next_w);
            i++;
        }
        else {
            busy++;
            std::this_thread::yield();
        }
    }
    p->stop();
    std::cout << "Busy: " << busy << "\n";
}


template <typename QueueType>
void producer(QueueType *queue)
{
    static size_t busy = 0;
    for (int i = 0; i < NumItems;) {
        typename QueueType::DataBucket data;
        data.size= 512;
        data.seq = i;
        std::size_t next_w;
        typename QueueType::DataBucketPtr data_ptr = nullptr;
#ifdef COPY
        if (queue->push_back(data)) {
            auto avail = queue->available();
            i++;
        }
        else {
            std::this_thread::yield();
        }
#else
        auto stat = queue->acquire_for_write(data_ptr, next_w);
        if (stat) {
            data_ptr->seq = i;
            queue->finish_write(next_w);
            i++;
        }
        else {
            busy++;
            std::this_thread::yield();
        }
#endif
    }
    std::cout << "Busy: " << busy << "\n";
}

void pool_thread(QueuePoolType *queue, size_t i) {
    auto pool = queue->pool(i);
    while(true) {
        typename QueueType::DataBucketPtr data_ptr;
        size_t next_r;
        while(!pool->acquire_for_read(data_ptr, next_r)) {
            std::this_thread::yield();
        }

        auto state = data_ptr->state;
        pool->finish_read(next_r);
        if(state == QueueType::DataBucket::STOP) break;
    }
}

void test_pool() {
        QueuePoolType pool;

        std::thread worker[QueuePoolType::NumWorkers];
        for(size_t i = 0; auto &w: worker) {
            w = std::thread(pool_thread, &pool, i);
            i++;
        }
        //std::thread producer(pool_producer, &pool);
        //producer.join();
        pool_producer(&pool);
        for(auto &w: worker) w.join();

};


template <typename QueueType>
void test_queue()
{
    QueueType queue;

    auto fut = std::async(std::launch::async, producer< QueueType >, &queue);

    for (int expected = 0; expected < NumItems; expected++) {
        int received = 0;
        typename QueueType::DataBucket bucket;
        typename QueueType::DataBucketPtr data_ptr;
        size_t next_r;
#ifdef COPY
        while (!queue.pop_front(bucket)) {
            std::this_thread::yield();
        }
#else
        while (!queue.acquire_for_read(data_ptr, next_r)) {
            std::this_thread::yield();
        }
#endif
#ifdef COPY
        received = bucket.seq;
#else
        received = data_ptr->seq;
        queue.finish_read(next_r);
#endif
        if (received != expected) {
            std::printf("Error: Expected %d, received %d\n", expected, received);
        }
    }

    fut.wait();
}

int main()
{
    using namespace std::chrono;
    auto t0 = steady_clock::now();
    test_queue< queue_spsc<int, 4096, 16384, policy::atomic> >();
    auto t1 = steady_clock::now();
   // test_queue< queue_spsc<int, 4096, 16384, policy::mutex> >();
    auto t2 = steady_clock::now();
    test_pool();
    auto t3 = steady_clock::now();
    auto d1 = t1 - t0;
    auto d2 = t2 - t1;
    auto d3 = t3 - t2;

    std::cout << "Lockless:" << duration<double, std::milli>(d1).count() << " ms" << std::endl;
    std::cout << "Locking:" << duration<double, std::milli>(d2).count() << " ms" << std::endl;
    std::cout << "Pool Queue:" << duration<double, std::milli>(d3).count() << " ms" << std::endl;

    return 0;
}
