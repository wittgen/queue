#pragma once

#include <array>
#include <atomic>
#include <memory>

namespace policy {
    struct mutex {
    };
    struct atomic {
    };
    struct none {
    };
}

template<typename T, size_t N>
struct DataBucket_ {
    DataBucket_ &operator=(const DataBucket_ &other) {
        if (this != &other) {
            size = other.size;
            seq = other.seq;
            std::copy(other.data, other.data + other.size, data);
        }
        return *this;
    }
    static constexpr size_t UNDEFINED = 0;
    static constexpr size_t PROCESS = 1;
    static constexpr size_t STOP = 2;
    size_t size{};
    size_t seq{};
    size_t state{PROCESS};
private:
    std::array<T, N> data_;
public:
    T *data = data_.data();
};

template<typename T, size_t Buckets = 1024, size_t BucketSize = 16384, typename Policy  = policy::atomic>
class queue_spsc {
public:
    using DataBucket = DataBucket_<T, BucketSize>;
    using DataBucketPtr = DataBucket *;

    queue_spsc() = default;

    std::size_t available() const {
        const size_t r = read_(mReadPos);
        const size_t w = read_(mWritePos);
        if (r > w)
            return Buckets - r + w;
        else
            return Buckets - w + r;
    }

    bool acquire_for_write(DataBucketPtr &item, size_t &next_w) {
        [[maybe_unused]] OptionalLock guard(mLock);
        const size_t r = read_(mReadPos);
        const size_t w = read_(mWritePos);
        next_w = increment(w);
        if (r == next_w) {
            item = nullptr;
            return false;
        }
        item = &mBuffer[w];
        return true;
    }

    void finish_write(size_t next_w) {
        [[maybe_unused]] OptionalLock guard(mLock);
        write_(mWritePos, next_w);
    }


    bool push_back(DataBucket const &item) {
        [[maybe_unused]] OptionalLock guard(mLock);

        const size_t r = read_(mReadPos);
        const size_t w = read_(mWritePos);
        const auto next_w = increment(w);
        if (r == next_w)
            return false;
        mBuffer[w] = item;
        write_(mWritePos, next_w);
        return true;
    }

    bool acquire_for_read(DataBucketPtr &item, size_t &pos) {
        [[maybe_unused]] OptionalLock guard(mLock);
        size_t const r = read_(mReadPos);
        size_t const w = read_(mWritePos);
        if (r == w) {
            item = nullptr;
            return false;
        }
        pos = increment(r);
        item = &mBuffer[r];
        return true;
    }

    void finish_read(size_t pos) {
        [[maybe_unused]] OptionalLock guard(mLock);
        write_(mReadPos, pos);
    }

    bool pop_front(DataBucket &item) {
        [[maybe_unused]] OptionalLock guard(mLock);
        size_t const r = read_(mReadPos);
        size_t const w = read_(mWritePos);
        if (r == w)
            return false;
        item = mBuffer[r];
        write_(mReadPos, increment(r));
        return true;
    }

private:
    static constexpr bool use_atomic_() {
        return std::is_same_v<Policy, policy::atomic>;
    }

    using IndexType = std::conditional_t<
            use_atomic_(),
            std::atomic<std::size_t>,
            std::size_t>;

    template<typename U>
    [[nodiscard]] std::size_t read_(U const &m) const {
        if constexpr (use_atomic_())
            return m.load(std::memory_order_consume);
        else return m;
    }

    template<typename U>
    void write_(U &m, std::size_t pos) {
        if constexpr (use_atomic_())
            m.store(pos, std::memory_order_release);
        else m = pos;
    }

    inline static auto increment = [](std::size_t pos) -> std::size_t {
        return (pos + 1) % Buckets;
    };

    struct NoLockGuard {
        template<class Mutex>
        explicit NoLockGuard(Mutex &) {}
    };

    struct NoMutex {
    };


    using OptionalLock = typename std::conditional_t<
            use_atomic_(),
            NoLockGuard,
            std::lock_guard<std::mutex>>;
    using OptionalMutex =
            typename std::conditional_t<
                    use_atomic_(),
                    NoMutex,
                    std::mutex>;
    using ArrayT = std::array<DataBucket, Buckets>;
    std::unique_ptr<ArrayT> mBuffer_
            = std::make_unique<ArrayT>();
    ArrayT &mBuffer = *mBuffer_.get();
    IndexType mReadPos{0};
    IndexType mWritePos{0};
    OptionalMutex mLock;
};
template<typename T>
concept Queue = requires(T a,
        T::DataBucket &bucketRef,
        T::DataBucket const &bucketCref,
        T::DataBucketPtr &bucketPtrRef,
        size_t size,
        size_t &sizeRef)
{
    { a.available() };
    { a.pop_front(bucketRef) } -> std::same_as<bool>;
    { a.push_back(bucketCref) } -> std::same_as<bool>;
    { a.finish_read(size) };
    { a.acquire_for_read(bucketPtrRef, sizeRef) } -> std::same_as<bool>;
    { a.acquire_for_write(bucketPtrRef, sizeRef) } -> std::same_as<bool> ;
};

template <Queue T, size_t N>
class queue_pool {
    public:
    using QueueType = T;
    static constexpr size_t NumWorkers = N;
    using QueueTypePtr = T*;
    bool getNext(QueueTypePtr &buffer) {
        buffer = &pool_[last_];
        last_++;
        if(last_>=N) last_=0;
        return true;
    }

    T* pool(size_t index) {
        return &pool_[index];
    }

    std::atomic_flag &flag(size_t index) {
        return flag_[index];
    }

    void stop() {
        for(auto &p:pool_) {
            typename QueueType::DataBucketPtr ptr;
            size_t ctx{};
            while(!p.acquire_for_write(ptr, ctx)) {};
            ptr->state = QueueType::DataBucket::STOP;
            p.finish_write(ctx);
        }
    }
private:
    bool _nextFree(size_t &index, size_t &avail) {
        for(int i = 0; auto const &p:pool_)
            available_[i++] = p.available();
        auto res = std::max_element(available_.begin(), available_.end());
        index = std::distance(available_.begin(), res);
        avail = available_[index];
        if(avail>0) {
            count_[index]++;
            sum_++;
            return true;
        }
        return false;

    }
    std::array<T, N> pool_{};
    std::array<size_t, N> available_{};
    std::array<size_t, N> count_{};
    std::array<std::atomic_flag, N> flag_{};
    std::size_t last_{};
    std::size_t sum_{};
};